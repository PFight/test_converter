#include <functional>
#include <vector>

class ResourceManager
{
    public:
        ResourceManager();
        ~ResourceManager();

        void disposeAll();

        void addDisposer(std::function<void()> disposer);

    private:
        std::vector<std::function<void()>> mDisposers;

        // Not copyable
        ResourceManager(const ResourceManager& copy);
        ResourceManager& operator=(const ResourceManager& assignee);
};