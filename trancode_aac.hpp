/*
 * This file is part of FFmpeg.
 *
 * FFmpeg is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * FFmpeg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FFmpeg; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
/**
 * @file
 * simple audio converter
 *
 * @example doc/examples/transcode_aac.c
 * Convert an input audio file to AAC in an MP4 container using FFmpeg.
 * @author Andreas Unterweger (dustsigns@gmail.com)
 */

/** The output bit rate in kbit/s */
#define OUTPUT_BIT_RATE 48000
/** The number of output channels */
#define OUTPUT_CHANNELS 2
/** The audio sample output format */
#define OUTPUT_SAMPLE_FORMAT AV_SAMPLE_FMT_S16

struct AVAudioFifo;
struct SwrContext;

namespace ffmpeg
{
    /**
     * Convert an error code into a text message.
     * @param error Error code to be converted
     * @return Corresponding error text (not thread-safe)
     */
    char *const get_error_text(const int error);

    /** Initialize one data packet for reading or writing. */
    void init_packet(AVPacket *packet);

    /** Initialize one audio frame for reading from the input file */
    int init_input_frame(AVFrame **frame);


    /**
     * Initialize the audio resampler based on the input and output codec settings.
     * If the input and output sample formats differ, a conversion is required
     * libswresample takes care of this, but requires initialization.
     */
    int init_resampler(AVCodecContext *input_codec_context,
                              AVCodecContext *output_codec_context,
                              SwrContext **resample_context);


    /** Initialize a FIFO buffer for the audio samples to be encoded. */
    int init_fifo(AVAudioFifo **fifo);

    /** Decode one audio frame from the input file. */
    int decode_audio_frame(AVFrame *frame,
                                  AVPacket* pinput_packet,
                                  AVCodecContext *input_codec_context,
                                  int *data_present);

    /**
     * Initialize a temporary storage for the specified number of audio samples.
     * The conversion requires temporary storage due to the different format.
     * The number of audio samples to be allocated is specified in frame_size.
     */
    int init_converted_samples(uint8_t ***converted_input_samples,
                                      AVCodecContext *output_codec_context,
                                      int frame_size);

    /**
     * Convert the input audio samples into the output sample format.
     * The conversion happens on a per-frame basis, the size of which is specified
     * by frame_size.
     */
    int convert_samples(const uint8_t **input_data,
                               uint8_t **converted_data, const int frame_size,
                               SwrContext *resample_context);

    /** Add converted input audio samples to the FIFO buffer for later processing. */
    int add_samples_to_fifo(AVAudioFifo *fifo,
                                   uint8_t **converted_input_samples,
                                   const int frame_size);

    /**
     * Read one audio frame from the input file, decodes, converts and stores
     * it in the FIFO buffer.
     */
    int read_decode_convert_and_store(AVAudioFifo *fifo,
                                             AVPacket* packet,
                                             AVCodecContext *input_codec_context,
                                             AVCodecContext *output_codec_context,
                                             SwrContext *resampler_context);

    /**
     * Initialize one input frame for writing to the output file.
     * The frame will be exactly frame_size samples large.
     */
    int init_output_frame(AVFrame **frame,
                                 AVCodecContext *output_codec_context,
                                 int frame_size);

    /** Encode one frame worth of audio to the output file. */
    int encode_audio_frame(AVFrame *frame,
                                  AVFormatContext *output_format_context,
                                  AVCodecContext *output_codec_context,
                                  int *data_present);

    /**
     * Load one audio frame from the FIFO buffer, encode and write it to the
     * output file.
     */
    int load_encode_and_write(AVAudioFifo *fifo,
                                     AVFormatContext *output_format_context,
                                     AVCodecContext *output_codec_context);
}