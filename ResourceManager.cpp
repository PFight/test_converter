#include "ResourceManager.hpp"


ResourceManager::ResourceManager()
{
    disposeAll();
}

ResourceManager::~ResourceManager()
{
    disposeAll();
}

void ResourceManager::disposeAll()
{
    for (auto disposerIt = mDisposers.rbegin(); disposerIt != mDisposers.rend(); disposerIt++)
    {
        (*disposerIt)();
    }
    mDisposers.clear();
}

void ResourceManager::addDisposer(std::function<void()> disposer)
{
    mDisposers.push_back(disposer);
}


// Not copyable
ResourceManager::ResourceManager(const ResourceManager& copy) {}
ResourceManager& ResourceManager::operator=(const ResourceManager& assignee) { return *this;}
