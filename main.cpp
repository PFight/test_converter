
#include <iostream>
#include <map>
#include <vector>


#define __STDC_CONSTANT_MACROS
extern "C" {


    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libavformat/avio.h>
    #include <libavutil/opt.h>
    #include <libavutil/audio_fifo.h>
    #include <libswresample/swresample.h>
}

#include "trancode_aac.hpp"
#include "ResourceManager.hpp"

void print_err(const char* msg, int err);

int main(int argc, char* argv[])
{
    if (argc <= 1)
    {
        std::cout << "Program use: ./converter <your_flv_file_path>" << std::endl;
        std::cout << "Missed input file path" << std::endl;
        return 0;
    }
    
    std::cout << "Processing file '" << std::string(argv[1]) << "'" << std::endl;

    ResourceManager disposer;

    avcodec_register_all();
    av_register_all();

    // Open input
    AVFormatContext* inputFormatContext;
    if (!(inputFormatContext = avformat_alloc_context())) {
        print_err("Can't alloc context", AVERROR(ENOMEM));
        return 0;
    }
    int ret = avformat_open_input(&inputFormatContext, argv[1], nullptr, nullptr);
    disposer.addDisposer([&]{ avformat_close_input(&inputFormatContext); });
    if (ret < 0)
    {
        avformat_close_input(&inputFormatContext);
        print_err("Can't open file", ret);
        return 0;
    }

    ret = avformat_find_stream_info(inputFormatContext, nullptr);
    if (ret < 0)
    {
        avformat_close_input(&inputFormatContext);
        print_err("Can't read streams info", ret);
        return 0;
    }

    // Open output
    AVFormatContext* outputFormatContext;
    std::string outputFileName(argv[1]);
    outputFileName += ".mp4";
    ret = avformat_alloc_output_context2(&outputFormatContext, nullptr, nullptr, outputFileName.c_str());
    disposer.addDisposer([=]{  avformat_free_context(outputFormatContext); });
    if (ret < 0)
    {
        print_err("Can't alloc output context", ret);
        return 0;
    }
    ret = avio_open(&outputFormatContext->pb, outputFileName.c_str(), AVIO_FLAG_WRITE);
    disposer.addDisposer([=]{ avio_close(outputFormatContext->pb); });
    if (ret < 0) {
        std::cout << "Could not open output file << '" << outputFileName << "'";
        return 0;
    }

    // Open codecs, create streams
    std::map<AVStream*, AVStream*> inputToOutputStream;
    std::vector<AVStream*> outputStreams;
    for (unsigned int i = 0; i < inputFormatContext->nb_streams; i++)
    {
        // Open decoder
        AVCodec* inputCodec = avcodec_find_decoder(inputFormatContext->streams[i]->codec->codec_id);
        if (!inputCodec)
        {
            std::cout << "Can't find input codec" << std::endl;
            continue;
        }

        ret = avcodec_open2(inputFormatContext->streams[i]->codec, inputCodec, nullptr);
        disposer.addDisposer([=]{  avcodec_close(inputFormatContext->streams[i]->codec); });
        if (ret < 0)
        {
            avformat_close_input(&inputFormatContext);
            print_err("Can't open decoder", ret);
            continue;
        }

        if (inputFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            // Open and setup output codec, create stream
            AVCodec* outputCodec = avcodec_find_encoder(AV_CODEC_ID_H264);
            if (outputCodec == nullptr)
            {
                std::cout << "Cant find H264 codec." << std::endl;
                return 0;
            }
            AVStream *outputStream = avformat_new_stream(outputFormatContext, outputCodec);
            if (outputStream == nullptr)
            {
                std::cout << "Cant create output stream" << std::endl;
                return 0;
            }
            inputToOutputStream[inputFormatContext->streams[i]] = outputStream;
            outputStreams.push_back(outputStream);

            outputStream->codec->pix_fmt = AV_PIX_FMT_YUV420P; //inputFormatContext->streams[i]->codec->pix_fmt;
            outputStream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER; // mp4 needs global headers %)
            outputStream->codec->width = inputFormatContext->streams[i]->codec->width;
            outputStream->codec->height = inputFormatContext->streams[i]->codec->height;
            outputStream->codec->time_base = {1, 25}; //inputFormatContext->streams[i]->codec->time_base;
            outputStream->codec->gop_size = inputFormatContext->streams[i]->codec->gop_size;
            outputStream->codec->bit_rate = inputFormatContext->streams[i]->codec->bit_rate;
            av_opt_set(outputStream->codec->priv_data, "preset", "slow", 0);

            ret = avcodec_open2(outputStream->codec, outputCodec, nullptr);
            disposer.addDisposer([=]{  avcodec_close(outputStream->codec); });
            if (ret < 0) {
                std::cout << "Could not open output codec";
                return 0;
            }
        }
        else if (inputFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            // Open and setup output codec, create stream        

            AVCodec* outputCodec = avcodec_find_encoder(AV_CODEC_ID_AAC);
            if (outputCodec == nullptr)
            {
                std::cout << "Cant find audio codec." << std::endl;
                return 0;
            }
            AVStream *outputStream = avformat_new_stream(outputFormatContext, outputCodec);
            if (outputStream == nullptr)
            {
                std::cout << "Cant create output stream" << std::endl;
                return 0;
            }
            
            inputToOutputStream.insert(std::make_pair(inputFormatContext->streams[i], outputStream));
            outputStreams.push_back(outputStream);
            
            outputStream->codec->strict_std_compliance = -2;
            outputStream->codec->channels = OUTPUT_CHANNELS;
            outputStream->codec->width = inputFormatContext->streams[i]->codec->width;
            outputStream->codec->height = inputFormatContext->streams[i]->codec->height;
            outputStream->codec->channel_layout = av_get_default_channel_layout(OUTPUT_CHANNELS);
            outputStream->codec->sample_rate = inputFormatContext->streams[i]->codec->sample_rate;
            outputStream->codec->bit_rate = inputFormatContext->streams[i]->codec->bit_rate;
            outputStream->codec->sample_fmt = AV_SAMPLE_FMT_FLTP; //inputFormatContext->streams[i]->codec->sample_fmt;
            outputStream->codec->flags |= CODEC_FLAG_GLOBAL_HEADER; // mp4 needs global headers %)
            outputStream->codec->time_base = inputFormatContext->streams[i]->codec->time_base;
            outputStream->codec->gop_size = inputFormatContext->streams[i]->codec->gop_size;

            ret = avcodec_open2(outputStream->codec, outputCodec, nullptr);
            disposer.addDisposer([=]{  avcodec_close(outputStream->codec); });
            if (ret < 0) {
                std::cout << "Could not open output codec";
                return 0;
            }
        }
    }
    
    av_dump_format(outputFormatContext, 0, "dump.txt", 1);

    // Write header
    ret = avformat_write_header(outputFormatContext, NULL);
    if (ret < 0)
    {
        print_err("Can't write header", ret);
        return 0;
    }

    // Transcode and write content
    AVFrame *frame = av_frame_alloc();
    disposer.addDisposer([&]{ av_frame_free(&frame); });
    AVFrame *audioFrame = av_frame_alloc();
    disposer.addDisposer([&]{ av_frame_free(&audioFrame); });
    AVPacket inputPacket = { 0 };
    av_init_packet(&inputPacket);
    AVPacket outputPacket = { 0 };
    av_init_packet(&outputPacket);
    int decodeFrameFinished = 0;
    while (av_read_frame(inputFormatContext, &inputPacket) >= 0)
    {      
        AVStream* inputStream = inputFormatContext->streams[inputPacket.stream_index];
        AVCodecContext* inputCodecContext = inputStream->codec;

        AVStream* outputStream = inputToOutputStream[inputStream];
        
        if (outputStream != nullptr)
        {
            //AVCodecContext* outputCodecContext = outputStream->codec;
            if (inputStream->codec->codec_type == AVMEDIA_TYPE_VIDEO)
            {
                // Decode frame
                ret = avcodec_decode_video2 (inputCodecContext, frame, &decodeFrameFinished, &inputPacket);
                if (ret >= 0)
                {
                    std::cout << "Frame decoded.." << std::endl;
                
                    if (decodeFrameFinished)
                    {
                        // Make copy of the frame, without any non-content disturbing stuff
                        AVFrame* newFrame = av_frame_alloc();											// Initialize a new frame
                        int size = avpicture_get_size(inputCodecContext->pix_fmt,
                            inputCodecContext->width, inputCodecContext->height);
                        uint8_t* picture_buf = (uint8_t*)av_mallocz(size);
                        avpicture_fill((AVPicture *)newFrame, picture_buf, inputCodecContext->pix_fmt,
                            inputCodecContext->width, inputCodecContext->height);
                        av_picture_copy((AVPicture*)newFrame, (AVPicture*)frame, inputCodecContext->pix_fmt,
                            inputCodecContext->width, inputCodecContext->height);

                        // Encode frame
                        int encodeFrameFinished = 0;
                        newFrame->format = outputStream->codec->pix_fmt;
                        newFrame->width  = outputStream->codec->width;
                        newFrame->height = outputStream->codec->height;
                        newFrame->pts= outputStream->codec->frame_number;
                        ret = avcodec_encode_video2(outputStream->codec,
                            &outputPacket, newFrame, &encodeFrameFinished);
                        outputPacket.stream_index = outputStream->index;
                        if (ret >= 0)
                        {
                            if (encodeFrameFinished)
                            {
                                std::cout << "Frame encoded" << std::endl;

                                // Write encoded frame to the file
                                ret = av_interleaved_write_frame(outputFormatContext, &outputPacket);
                                if (ret >= 0 && encodeFrameFinished)
                                {
                                    std::cout << "Frame writed" << std::endl;
                                }
                                else
                                {
                                    print_err("Error write", ret);
                                }
                            }
                            else
                            {
                                std::cout << "Not finished yet. " << std::endl;
                            }
                        }
                        else
                        {
                            print_err("Error encode", ret);
                        }
                    }
                }
                else
                {
                    print_err("Error decode", ret);
                }

            } // if (inputStream->codec->codec_type == AVMEDIA_TYPE_VIDEO)
            //Code below leads to crash..
            /*else if (inputStream->codec->codec_type == AVMEDIA_TYPE_AUDIO)
            {
                ret = avcodec_decode_audio4(inputCodecContext, audioFrame, &decodeFrameFinished, &inputPacket);
                if (ret >= 0)
                {
                    std::cout << "aFrame decoded.." << std::endl;
                
                    if (decodeFrameFinished)
                    {
                        // Make copy of the frame, without any non-content disturbing stuff
                        AVFrame* newFrame = audioFrame; //av_frame_alloc();

                        // Initialize a new frame
                        //int size = avpicture_get_size(inputCodecContext->pix_fmt,
                        //    inputCodecContext->width, inputCodecContext->height);
                        //uint8_t* picture_buf = (uint8_t*)av_mallocz(size);
                        //avpicture_fill((AVPicture *)newFrame, picture_buf, inputCodecContext->pix_fmt,
                        //    inputCodecContext->width, inputCodecContext->height);
                        //av_picture_copy((AVPicture*)newFrame, (AVPicture*)frame, inputCodecContext->pix_fmt,
                       //     inputCodecContext->width, inputCodecContext->height);

                        newFrame->nb_samples     = outputStream->codec->frame_size;
                        newFrame->format         = outputStream->codec->sample_fmt;
                        newFrame->channel_layout = outputStream->codec->channel_layout;

                        // Encode frame
                        int encodeFrameFinished = 0;
                        newFrame->pts= outputStream->codec->frame_number;
                        ret = avcodec_encode_audio2(outputStream->codec,
                            &outputPacket, newFrame, &encodeFrameFinished);
                        outputPacket.stream_index = outputStream->index;
                        if (ret >= 0)
                        {
                            if (encodeFrameFinished)
                            {
                               std::cout << "aFrame encoded" << std::endl;

                               // Write encoded frame to the file
                               ret = av_interleaved_write_frame(outputFormatContext, &outputPacket);
                               if (ret >= 0 && encodeFrameFinished)
                               {
                                   std::cout << "aFrame writed" << std::endl;
                               }
                               else
                               {
                                   print_err("aError write", ret);
                               }
                            }
                            else
                            {
                                std::cout << "aNot finished yet. " << std::endl;
                                //av_free_packet(&outputPacket);
                                //av_free_packet(&inputPacket);
                                continue;
                            }
                        }
                        else
                        {
                            print_err("aError encode", ret);
                        }
                    }
                }
                else
                {
                    print_err("aError decode", ret);
                }
            } // else if (inputStream->codec->codec_type == AVMEDIA_TYPE_AUDIO)
            */
            av_free_packet(&outputPacket);
            av_free_packet(&inputPacket);
        } // if (outputStream != nullptr)
    } // while (av_read_frame(inputFormatContext, &inputPacket) >= 0)

    // Write delayed frames
    for (AVStream* outputStream : outputStreams)
    {
        for (int got_output = 1; got_output != 0;)
        {
            got_output = 0;
            if (outputStream->codec->codec_type == AVMEDIA_TYPE_VIDEO)
            {
                ret = avcodec_encode_video2(outputStream->codec, &outputPacket, NULL, &got_output);                    
                if (ret < 0) {
                    fprintf(stderr, "Error encoding frame\n");
                    return 0;
                }
                outputPacket.stream_index = outputStream->index;
                if (got_output)
                {
                    ret = av_interleaved_write_frame(outputFormatContext, &outputPacket);
                    if (ret >= 0)
                    {
                        std::cout << "Delayed frame writed" << std::endl;
                    }
                    else
                    {
                        print_err("Error write", ret);
                    }
                    av_free_packet(&outputPacket);
                }
            }
            else if (outputStream->codec->codec_type == AVMEDIA_TYPE_AUDIO)
            {
                ret = avcodec_encode_audio2(outputStream->codec, &outputPacket, NULL, &got_output);
                if (ret < 0) {
                    fprintf(stderr, "Error encoding frame\n");
                    return 0;
                }
                outputPacket.stream_index = outputStream->index;
                if (got_output)
                {
                    ret = av_interleaved_write_frame(outputFormatContext, &outputPacket);
                    if (ret >= 0)
                    {
                        std::cout << "Delayed frame writed" << std::endl;
                    }
                    else
                    {
                        print_err("Error write", ret);
                    }
                    av_free_packet(&outputPacket);
                }
            }
            
        } // for (int got_output = 1; got_output != 0;)
    } // for (AVStream* outputStream : outputStreams)

    // Write trailer
    av_write_trailer(outputFormatContext);

    // Cleanup
    disposer.disposeAll();
}

void print_err(const char* msg, int err)
{
    if (err < 0)
    {
        std::cout << msg << ": " << std::endl;
        char buf[AV_ERROR_MAX_STRING_SIZE];
        av_make_error_string(buf, AV_ERROR_MAX_STRING_SIZE, err);
        std::cout << buf << std::endl;
    }
}
